angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});


})


.controller('MainCtrl', function($scope, $stateParams,$rootScope,$http,$ionicSideMenuDelegate,$ionicPopup,$timeout) {
	
	
	$ionicSideMenuDelegate.canDragContent(false);
	$scope.showSplashImage = true;
	
	$scope.Host = $rootScope.Host;
	$scope.PHPHost = $rootScope.PHPHost;


		 $timeout(function() {
			$scope.showSplashImage = false;
		}, 5000);

		
	
	/*
	var ShowMainImage = $ionicPopup.show({
	  //template: '',
	  title: '<div align="center" style="width:100% !important; "><img src="img/shaar.jpg" style="width:100%;"></div>',
	  cssClass: 'yourclass',
	  //subTitle: 'MySubTitle',
	});
	
	//ShowMainImage.close();
	*/
	
	
$scope.$on('$ionicView.enter', function(e) {
  

  
	$scope.GalleryArray = [];
	
	$scope.getGallery = function()
	{
		$http.get($rootScope.Host+'/GetGallery')
		.success(function(data, status, headers, config)
		{
			
			//$scope.unread = data.length;
			$scope.GalleryArray = data;
			//console.log('Gallery: ', data);
			
			$scope.Random = Math.floor(Math.random()*data.length);
			$scope.randomImage = $scope.GalleryArray[$scope.Random];
			$rootScope.loadOnceGallery = 1;
			//alert ($scope.randomImage)
			//alert ($scope.Rand);
			

		})
		.error(function(data, status, headers, config)
		{

		});			
	}
	
	//if ($rootScope.loadOnceGallery == 0)
	$scope.getGallery();

});

	

	$scope.getBlog = function()
	{
		//$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
		
		data = 
		{
			"id" : $scope.Catagory
		}

		
		$http.get($rootScope.Host+'/getBlog')
		.success(function(data, status, headers, config)
		{
			//$scope.blogArray = data;
			$rootScope.blogArray = data;
			$rootScope.loadOnceBlog = 1;
			console.log("BlogArray: " , data);
			

		})
		.error(function(data, status, headers, config)
		{

		});			
	}
	
	//if ($rootScope.loadOnceBlog == 0)
		$scope.getBlog();

	

	$scope.navigateArticles = function(id)
	{
		//alert (id);
		window.location = "#/app/list/"+id;
	}



		
	
	
})

.controller('ListCtrl', function($scope, $stateParams,$rootScope,$http,$ionicSideMenuDelegate,$ionicScrollDelegate,$ionicLoading) {
	
	$ionicSideMenuDelegate.canDragContent(false);
	
	$scope.Host = $rootScope.Host;
	$scope.PHPHost = $rootScope.PHPHost;
	$scope.Catagory = $stateParams.ItemId;
	$scope.imageArray = new Array("1.png","2.png","3.png","4.png","5.png","6.png","7.png");

	$scope.blurSearch = function()
	{
		$ionicScrollDelegate.scrollTop();
	}
	//$scope.randomImage = $scope.imageArray[0];
	//alert ($scope.Catagory);

	$scope.getArticles = function()
	{
		
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});

				
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
		
		data = 
		{
			"id" : $scope.Catagory
		}

		
		$http.post($rootScope.Host+'/getArticles',data)
		.success(function(data, status, headers, config)
		{
			$ionicLoading.hide();
			//$scope.articlesArray = [];
			//$rootScope.ArticlesArray = [];
			
			$scope.articlesArray = data;
			$rootScope.ArticlesArray = $scope.articlesArray;
			//console.log("Articles: " , data);
			

		})
		.error(function(data, status, headers, config)
		{
			$ionicLoading.hide();
		});			
	}
	
	$scope.getArticles();
		
	
	$scope.goArticle = function(index)
	{
		window.location = "#/app/details/"+index;
	}
	
})


.controller('DetailsCtrl', function($scope, $stateParams,$rootScope,$http,$ionicSideMenuDelegate,$ionicModal,$sce,$ionicPlatform,$timeout,$ionicHistory) {

	$scope.Host = $rootScope.Host;
	$scope.PHPHost = $rootScope.PHPHost;
	$scope.ArticleId = $stateParams.ItemId;
	$scope.ArticleData = [];
	//$scope.ArticleData = $rootScope.ArticlesArray[$scope.ArticleId];
	
	for(i=0;i<$rootScope.ArticlesArray.length;i++)
	{
		if($rootScope.ArticlesArray[i].index == $scope.ArticleId)
		$scope.ArticleData = $rootScope.ArticlesArray[i];
	}
	//alert ($scope.ArticleData);
	console.log("Article : " , $scope.ArticleData)
	$scope.Random = Math.floor(Math.random()*12)+1;
	$scope.RandomImage = 'img/main/'+$scope.Random+'.png';

	$scope.phone = function(tel)
	{
		if (!tel)
			return;
		
		$scope.telArray = tel.split("-")
		if($scope.telArray.length > 0 )
		$scope.tel = $scope.telArray[0]+ "" + $scope.telArray[1]; 
		else
		$scope.tel = $scope.telArray[0];
		//$scope.tel = $scope.tel.substr(1);
		//$scope.tel = "972"+$scope.tel
		
		//alert($scope.tel)
		window.location ="tel://"+$scope.tel;
		//window.loctaion="tel:"+$scope.tel;
	}
	
	$scope.openMail = function(mail)
	{
		if (mail)
			window.location = "mailto:"+mail;	
	}

	$scope.openShare = function(item)
	{
		window.plugins.socialsharing.share(item.migdal+' ' +item.phone+ ' '+item.address  , 'אפליקציית תמרים')
	}	
	
	$ionicPlatform.registerBackButtonAction(function (event) 
	{
		 $timeout(function() {
			window.history.back();
			$scope.stopVideo();
			$scope.closeVideoModal();
			
			//$ionicHistory.goBack();
		}, 300);
		
	},300);

	
	
	
	$scope.Video = function()
	{
		if ($scope.ArticleData.video)
		{

			$scope.VideoLink = $sce.trustAsResourceUrl($scope.ArticleData.video);

			$ionicModal.fromTemplateUrl('video-modal.html', {
			  scope: $scope,
			  animation: 'slide-in-up'
			}).then(function(VideoModal) {
			  $scope.VideoModal = VideoModal;
			  $scope.VideoModal.show();
			 });

			/*	
			$ionicModal.fromTemplateUrl('templates/video_modal.html', {
			  scope: $scope,
			  animation: 'slide-in-up'
			}).then(function(VideoModal) {
			  $scope.VideoModal = VideoModal;
			  $scope.VideoModal.show();
			 });			
			*/
		}
		else
		{
			$ionicPopup.alert({
				 title: 'לא נמצא סרטון יש לנסות מאוחר יותר',
				 template: ''
			   });			
		}
		

		 
		//window.location  = ;
	}
	
	$scope.stopVideo = function()
	{
		var div = document.getElementById('videoWrapper');
		var player = document.getElementById('VideoPlayer');
		div.innerHTML = '';
		div.remove();
		player.src='';

	}

			
	$scope.closeVideoModal= function()
	{
		$scope.VideoModal.hide();
		$scope.stopVideo();
	}
	
	

})


.controller('BlogPostsCtrl', function($scope, $stateParams,$rootScope,$http,$ionicSideMenuDelegate) {

	$scope.Host = $rootScope.Host;
	$scope.PHPHost = $rootScope.PHPHost;
	$scope.blogPosts = $rootScope.blogArray;
	//alert ($scope.blogPosts)

	//alert ($scope.ArticleData)
	

})



.controller('BlogDetailsCtrl', function($scope, $stateParams,$rootScope,$http,$ionicSideMenuDelegate) {

	$scope.Host = $rootScope.Host;
	$scope.PHPHost = $rootScope.PHPHost;
	$scope.BlogId = $stateParams.ItemId;
	$scope.ArticleContent = $rootScope.blogArray[$scope.BlogId];
	//alert ($scope.BlogId)

	
	
	 

})

.controller('ContactCtrl', function($scope, $stateParams,$rootScope,$http,$ionicSideMenuDelegate,$ionicPopup,$ionicLoading) {

	$scope.contact = 
	{
		"name" : "",
		"email" : "",
		"desc" : ""
	}

	$scope.sendContact = function()
	{

		
		
		var emailRegex = /\S+@\S+\.\S+/;
        if ($scope.contact.name == "") 
		{

        }

		
        else if ($scope.contact.email == "") 
		{

        } else if (emailRegex.test($scope.contact.email) == false) {

		   $scope.contact.email =  '';
        } 
		else if ($scope.contact.desc == "") 
		{

        } else 
		{
			
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});

					
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
			
			data = 
			{
				"name" : $scope.contact.name,
				"email" : $scope.contact.email,
				"desc" : $scope.contact.desc
			}

			
			$http.post($rootScope.Host+'/SendContact',data)
			.success(function(data, status, headers, config)
			{
				$ionicLoading.hide();


			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});	

			
				
				
				
				$scope.contact.name = '';
				$scope.contact.email = '';
				$scope.contact.desc = '';
			
				$ionicPopup.alert({
				title: 'תודה, פניתך התקבלה בהצלחה, נחזור אליך בהקדם',
				buttons: [{
				text: 'אישור',
				type: 'button-positive',
				  }]
			   });
			   
			   window.location = "#/app/main";
		}
	}

})


.filter('randomImageFilter', function ($rootScope) {
    return function (value) 
	{

		var Random = Math.floor(Math.random()*7)+1;
		return 'img/random/'+Random+'.png';
		
    };
})


.filter('toTrusted', function ($sce) 
{
    return function (value) {
        return $sce.trustAsHtml(value);
    };
})