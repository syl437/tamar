// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers'])

.run(function($ionicPlatform,$rootScope,$state) {
	
	$rootScope.Host = 'http://tapper.co.il/tmarim/laravel/public';
	$rootScope.PHPHost = 'http://tapper.co.il/tmarim/php/';
	$rootScope.ArticlesArray = [];
	$rootScope.blogArray = [];
	$rootScope.loadOnceGallery = 0;
	$rootScope.loadOnceBlog = 0;
	$rootScope.currState = $state;
	$rootScope.State = '';	
	
	$rootScope.$watch('currState.current.name', function(newValue, oldValue) {
      $rootScope.State = newValue;
    });  
	
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
	$ionicConfigProvider.backButton.previousTitleText(false).text('');
	
	
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })



    .state('app.main', {
      url: '/main',
      views: {
        'menuContent': {
          templateUrl: 'templates/main.html',
          controller: 'MainCtrl'
        }
      }
    })

	
    .state('app.list', {
      url: '/list/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/list.html',
          controller: 'ListCtrl'
        }
      }
    })

    .state('app.details', {
      url: '/details/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/details.html',
          controller: 'DetailsCtrl'
        }
      }
    })

    .state('app.blogposts', {
      url: '/blogposts/:blogposts',
      views: {
        'menuContent': {
          templateUrl: 'templates/blogposts.html',
          controller: 'BlogPostsCtrl'
        }
      }
    })	

	
    .state('app.blogdetails', {
      url: '/blogdetails/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/blogdetails.html',
          controller: 'BlogDetailsCtrl'
        }
      }
    })	

    .state('app.contact', {
      url: '/contact',
      views: {
        'menuContent': {
          templateUrl: 'templates/contact.html',
          controller: 'ContactCtrl'
        }
      }
    })	
	
	
;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/main');
});
